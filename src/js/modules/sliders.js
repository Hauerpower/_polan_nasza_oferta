export  default function slidersInit(){
	
	(function($){
	
		$(document).ready(function(){
			$('.related-wrapper').slick({
				arrows: true,
				infinite: true,
				speed: 300,
				slidesToShow: 5,
				slidesToScroll: 1,
				responsive: [
					{
						breakpoint: 1224,
						settings: {
						  slidesToShow: 4,
						  slidesToScroll: 1,

						  arrows: true
						}
					  },
					{
						breakpoint: 768,
						settings: {
						  slidesToShow: 3,
						  slidesToScroll: 1,

						  arrows: true
						}
					  },
				  {
					breakpoint: 640,
					settings: {
					  slidesToShow: 2,
					  slidesToScroll: 1,
					  arrows: true
					}
				  },
				  {
					breakpoint: 480,
					settings: {
					  slidesToShow: 2,
					  slidesToScroll: 1,
					  arrows: true
					}
				  }
				  // You can unslick at a given breakpoint now by adding:
				  // settings: "unslick"
				  // instead of a settings object
				]
			  });
		  });

	})(jQuery);
	
}
 